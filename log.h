#ifndef LOG_H
#define LOG_H

class LogFile {
	private:
		int			nodeID;
		string			fName;
	
	public:
		LogFile ();
		LogFile (int, string);
		
		void append (string);
};

#endif