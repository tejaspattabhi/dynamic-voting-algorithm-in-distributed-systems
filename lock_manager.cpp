#include <iostream>
#include <fstream>
using namespace std;
#include <random>
#include <queue>
#include <string>
#include <cstring>
#include <chrono>
#include <algorithm>

#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <pthread.h>

#include "log.h"
#include "lock_manager.h"

#define	SIZE			100
#define	SOCKET_ERROR	-1

#define	LOCKED			1
#define	UNLOCKED		0

#define	SHARED			1
#define	EXCLUSIVE		2
#define	IDLE			0

#define	BUSY			10
#define	OKAY			11
#define	NO_QUORUM		12

#define	ALIVE			1
#define	DEAD			0

#define	READ			1
#define	WRITE			0
#define NONE			-1

#define	DELAY			5000

/*Node member functions description*/
Node::Node () {
	nodeID = -1;
	hostname = "";
	port = -1;
}

Node::Node (int nID) {
	nodeID = nID;
	hostname = "";
	port = -1;
	
	life = DEAD;
}

Node::Node (int nID, string hName, int pNum) {
	nodeID = nID;
	hostname = hName;
	port = pNum;
	
	life = ALIVE;
}

int Node::getNodeID () {
	return nodeID;
}

string Node::getHostName () {
	return hostname;
}

int Node::getPort () {
	return port;
}

void 	Node::setLife (int l) {
	life = l;
}

int		Node::getLife () {
	return life;
}

/*Class and Member functions of NodeStatus*/
NodeStatus::NodeStatus () {
	RU		=	0;
	VN		=	0;
	DN		=	-1;
	
}

NodeStatus::NodeStatus (int nID, string hName, int pNum) : Node (nID, hName, pNum) {
	RU		=	0;
	VN		=	0;
	DN		=	-1;
}

NodeStatus::NodeStatus (int RU, int VN, int DN) {
	this->RU		=	RU;
	this->VN		=	VN;
	this->DN		=	DN;
}

int 	NodeStatus::getRU () {
	return RU;
}

void 	NodeStatus::setRU (int RU) {
	this->RU = RU;
}
	
int 	NodeStatus::getVN () {
	return VN;
}

void 	NodeStatus::setVN (int VN) {
	this->VN = VN;
}
	
int 	NodeStatus::getDN () {
	return DN;
}

void 	NodeStatus::setDN (int DN) {
	this->DN = DN;
}

/*Class and Member functions of LockManager*/
LockManager::LockManager () {
	nodeCount 	= 	0;
	nodeID 		=	-1;
	
	RU			= 	nodeCount;
	VN			=	0;
	DN			=	-1;
	
	QCount		=	0;
	nFiles		= 	0;
	count 		=	0;
	
	selfRequest	= 	NONE;
	hostname	=	"";
	port		=	-1;
	
	for (int i = 0; i < SIZE; i++)
		locker[i] = IDLE;
	
	
	lock = IDLE;
	
	pthread_mutex_init(&lockMutex, NULL);	
	pthread_mutex_init(&lockerMutex, NULL);
}

LockManager::LockManager (LogFile rec, int nodeID, string fName) {
	ifstream 	ifile;
	char 		buffer [SIZE * 5], temp [SIZE], value [SIZE];
	int 		i;
	
	record = rec;
	count = 0;
	
	for (int i = 0; i < SIZE; i++)
		locker[i] = IDLE;
	
	selfRequest = NONE;
	lock = IDLE;
	this->nodeID = nodeID;
	ifile.open (fName);
	
	// Read Config File
	ifile >> buffer;
	if (ifile.eof ()) {
		record.append ("Config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			record.append ("config file error");
			exit (1);
		}		
	}
	
	// Buffer has node count
	nodeCount = atoi (buffer);
	record.append ("Node Count - " + to_string (nodeCount));
	
	ifile >> buffer;
	if (ifile.eof ()) {
		record.append ("config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			record.append ("config file error");
			exit (1);
		}		
	}
	
	ifile >> temp >> value;
	if (ifile.eof ()) {
		record.append ("config file error");
		exit (1);
	}
	
	nodes[0] = Node (atoi (buffer), string (temp), atoi (value));
	record.append ("Node 0: " + string (buffer) + ", " + string (temp) + ", " + string (value));
	for (i = 1; i < nodeCount; i++) {
		ifile >> buffer >> temp >> value;
		record.append ("Node " + to_string (i) + ": " + string (buffer) + ", " + string (temp) + ", " + string (value));
		if (ifile.eof ()) {
			record.append ("config file error");
			exit (1);
		}		
		nodes[i] = Node (atoi (buffer), string (temp), atoi (value));
	}
	
	ifile >> buffer;
	if (ifile.eof ()) {
		record.append ("config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			record.append ("config file error");
			exit (1);
		}		
	}
	
	// buffer has number of files in the system now.
	nFiles = atoi (buffer);
	record.append ("Number of files: " + to_string (nFiles));
	
	// Update Values
	RU			= 	nodeCount;
	VN			=	0;
	DN			=	-1;
	
	record.append ("Variables (RU, VN, DN): " + to_string (RU) + ", " + to_string (VN) + ", " + to_string (DN));
	
	// get current system info
	hostname = nodes[nodeID].getHostName ();
	port = nodes[nodeID].getPort ();
	
	ifile.close ();
	
	pthread_mutex_init(&lockMutex, NULL);
	pthread_mutex_init(&lockerMutex, NULL);
}

int		LockManager::getMyPort () {
	return port;
}

string	LockManager::getMyHostName () {
	return hostname;
}

int	LockManager::getNodeCount () {
	return nodeCount;
}

Node *	LockManager::getNode (int i) {
	return &nodes[i];
}

int 	LockManager::getRU () {
	return RU;
}

void 	LockManager::setRU (int RU) {
	this->RU = RU;
}
	
int 	LockManager::getVN () {
	return VN;
}

void 	LockManager::setVN (int VN) {
	this->VN = VN;
}
	
int 	LockManager::getDN () {
	return DN;
}

void 	LockManager::setDN (int DN) {
	this->DN = DN;
}

void * 	LockManager::serverThread (void * ptr) {
	LockManager * lm;
	char 		buffer [SIZE];
	char 		temp [SIZE];
	char 		msg [SIZE];
	int			msgCount, i;
	Node 		node;
	
	// TCP Variables
	int 		hSocket, hServerSocket;
	struct 		sockaddr_in 	sAddress;
	int 		nAddressSize;
	
	struct 		hostent * 		pHostInfo;
    struct 		sockaddr_in 	Address;
    long 		nHostAddress;
	char *		str;
	
	lm 				= 	(LockManager *) ptr;
	nAddressSize 	= 	sizeof (struct sockaddr_in);
	str 			= 	NULL;
	msgCount		= 	0;
	i				= 	0;
	
	hServerSocket = socket (AF_INET, SOCK_STREAM, 0);
	if (hServerSocket == SOCKET_ERROR)	{
		lm->record.append ("Server: Could not make a socket");
		exit (1);
	}

	lm->record.append ("My Hostname is " + lm->getMyHostName () + " and my port number is " + to_string (lm->getMyPort ()));
	
	memset (&sAddress, 0, sizeof (sAddress));
	bzero ((void *) &sAddress, sizeof (sAddress));
	sAddress.sin_addr.s_addr 	= 	htonl (INADDR_ANY);
	sAddress.sin_port 			= 	htons (lm->getMyPort ());
	sAddress.sin_family 		= 	AF_INET;

	if (bind (hServerSocket, (struct sockaddr *) &sAddress, sizeof (sAddress)) == SOCKET_ERROR)	{
		lm->record.append ("Server: Could not bind");
		close (hServerSocket);
		exit (1);
	}

	getsockname (hServerSocket, (struct sockaddr *) &sAddress, (socklen_t *) &nAddressSize);
	if (listen (hServerSocket, SIZE) == SOCKET_ERROR)    {
		lm->record.append ("Server: Could not listen");
		close (hServerSocket);
		exit (1);
	}
	
	lm->record.append ("Server: Ready for listening");
		
	while (true) {
		// Read Message to msg
		hSocket = accept (hServerSocket, (struct sockaddr*) &sAddress, (socklen_t *) &nAddressSize);
		if (hSocket == SOCKET_ERROR)	{
			lm->record.append ("Server: Failed Accepting..");
			exit (1);
		}
		// lm->record.append ("Server: Accepted a connection");
		
		// read and write using hSocket
		memset (msg, 0, SIZE); 
		read (hSocket, msg, SIZE);
		
		if (!strcmp (msg, "")) {
			lm->record.append ("ERROR! - Server noticed null string");
			close (hSocket);
			continue;
		}
		
		msg [SIZE] = '\0';
		strcpy (temp, msg);
		
		lm->record.append ("Message Received: " + string (msg));
		
		str = strtok (temp, "|");
		if (string (str) == "ARE_YOU_ALIVE#") {
			memset (msg, 0, SIZE);
			sprintf (msg, "I_AM_ALIVE#");
			// lm->record.append ("Replying: " + string (msg));
			write (hSocket, msg, strlen (msg));
		}
		else if (string (str) == "REQUEST_SHARED_LOCK") {
			int nID, tCount;
			sscanf (msg, "REQUEST_SHARED_LOCK|%d|%d#", &nID, &tCount);
			
			memset (msg, 0, SIZE);
			if (lm->canGiveSharedLock (tCount, nID))
				sprintf (msg, "SHARED_LOCK_GRANTED|%d|%d|%d#", lm->getRU (), lm->getVN (), lm->getDN ());
			else
				sprintf (msg, "REQUEST_DENIED#");
			lm->record.append ("Replying: " + string (msg));
			write (hSocket, msg, strlen (msg));			
		}
		else if (string (str) == "REQUEST_EXCLUSIVE_LOCK") {
			int nID, tCount;
			sscanf (msg, "REQUEST_EXCLUSIVE_LOCK|%d|%d#", &nID, &tCount);
			
			memset (msg, 0, SIZE);
			if (lm->canGiveExclusiveLock (tCount, nID))
				sprintf (msg, "EXCLUSIVE_LOCK_GRANTED|%d|%d|%d#", lm->getRU (), lm->getVN (), lm->getDN ());
			else
				sprintf (msg, "REQUEST_DENIED#");
			lm->record.append ("Replying: " + string (msg));
			write (hSocket, msg, strlen (msg));
		}
		else if (string (str) == "RELEASE_SHARED_LOCK") {
			int nID;
			
			sscanf (msg, "RELEASE_SHARED_LOCK|%d#", &nID);
			lm->releaseLocker (nID);
			
			memset (msg, 0, SIZE);
			sprintf (msg, "THANK_YOU#");
			lm->record.append ("Replying: " + string (msg));
			write (hSocket, msg, strlen (msg));
		}
		else if (string (str) == "RELEASE_EXCLUSIVE_LOCK") {
			lm->record.append ("msg: " + string (msg));
			
			int nID, tRU, tVN, tDN;
			sscanf (msg, "RELEASE_EXCLUSIVE_LOCK|%d|%d|%d|%d#", &nID, &tRU, &tVN, &tDN);
			
			if (tRU >= 0)
				lm->setRU (tRU);
			
			if (tVN >= 0)
				lm->setVN (tVN);
			
			if (tDN >= 0)
				lm->setDN (tDN);
			
			lm->releaseLocker (nID);
			
			memset (msg, 0, SIZE);
			sprintf (msg, "THANK_YOU#");
			lm->record.append ("Replying: " + string (msg));
			write (hSocket, msg, strlen (msg));
		}
		else {
			// If any other message, just discard it
		}
		
		close (hSocket);
	}
}

int 	LockManager::getSharedLock () {
	int 	hSocket;
    struct 	hostent * 		pHostInfo;
    struct 	sockaddr_in 	Address;
    long 	nHostAddress;
	char 	msg [SIZE], temp [SIZE];
	char 	* str;
	
	record.append ("Requested for Shared Lock - Count " + to_string (count));
	
	P.clear ();
	Q.clear ();
	selfRequest = SHARED;
	str = NULL;
	
	// check locker for any EXCLUSIVE LOCKS
	pthread_mutex_lock(&lockerMutex);
	for (int i = 0; i < nodeCount; i++) {
		if (locker [i] == EXCLUSIVE) {
			pthread_mutex_unlock(&lockerMutex);
			return BUSY;
		}
	}
	pthread_mutex_unlock(&lockerMutex);
	
	// tell the object that I am sending messages and to FREEZE all other +ve responses on server by setting 'lock'
	
	// send messages to all nodes requesting for a lock
	for (int i = 0; i < nodeCount; i++) {
		if (i != nodeID) {
			hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (hSocket == SOCKET_ERROR)	{
				record.append ("getSharedLock: Could not make a socket");
				exit (1);
			}
			
			// record.append ("Requesting Shared access from node " + to_string (nodes[i].getNodeID ()));
			
			pHostInfo = gethostbyname ((nodes[i].getHostName ()).c_str ());
			memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);

			Address.sin_addr.s_addr = 	nHostAddress;
			Address.sin_port 		= 	htons (nodes[i].getPort ());
			Address.sin_family 		= 	AF_INET;	
			
			// record.append ("Connecting to - " + to_string (nodes[i].getNodeID ()) + ", " + nodes[i].getHostName () + ", " + to_string (nodes[i].getPort ()));
			if (connect (hSocket, (struct sockaddr *) &Address, sizeof (Address)) == SOCKET_ERROR)	{
				record.append ("getSharedLock: Failed to connect to " + to_string (nodes[i].getNodeID ()));
				// exit (1);
			}
			else {
				str = NULL;
				do {
					memset (msg, 0, SIZE);
					sprintf (msg, "REQUEST_SHARED_LOCK|%d|%d#", this->getNodeID (), count);
					write (hSocket, msg, strlen (msg));
					
					record.append ("Message Sent to " + to_string (nodes[i].getNodeID ()) + " :  " + string (msg));
					
					memset (msg, 0, SIZE);
					read (hSocket, msg, SIZE);
					
					msg[SIZE] = '\0';
					if (!strcmp (msg, ""))
						break;
					
					// as per the replies, create an object of NodeStatus, initialize it and push_back on P
					record.append ("Message Received from " + to_string (nodes[i].getNodeID ()) + " :  " + string (msg));
					strcpy (temp, msg);
					
					str = NULL;
					str = strtok (temp, "|");
					
					// usleep (DELAY);
				} while (string (str) != "REQUEST_DENIED#" && string (str) != "SHARED_LOCK_GRANTED");
				
				if (strcmp (msg, "") && string (str) == "SHARED_LOCK_GRANTED") {
					int tRU, tVN, tDN;
					sscanf (msg, "SHARED_LOCK_GRANTED|%d|%d|%d#", &tRU, &tVN, &tDN);
				
					NodeStatus ns = NodeStatus (nodes[i].getNodeID (), nodes[i].getHostName (), nodes[i].getPort ());
					ns.setRU (tRU);
					ns.setVN (tVN);
					ns.setDN (tDN);
					
					P.push_back (ns);
				}
			} 
			
			// Close TCP Connection
			if (close (hSocket) == SOCKET_ERROR)	{
				record.append ("getSharedLock: Could not close a socket");
			}
		}
		else {
			NodeStatus ns = NodeStatus (getNodeID (), nodes[getNodeID ()].getHostName (), nodes[getNodeID ()].getPort ());
			ns.setRU (RU);
			ns.setVN (VN);
			ns.setDN (DN);
			
			P.push_back (ns);
		}
	}
	int grant = NONE;
	// check if I have a my Quorum
	/*Algorithm: Based on the result, set grant to OKAY or NO_QUORUM*/
	
	if (P.size () > 1) {
		// Calculate M
		M = -1;
		for (vector<NodeStatus>::iterator node = P.begin (); node < P.end (); node++) {
			if (M < node->getVN ())
				M = node->getVN ();
		}
		
		// Compute Q
		string rec = "";
		Q.clear ();
		for (vector<NodeStatus>::iterator node = P.begin (); node < P.end (); node++) {
			if (node->getVN () == M) {
				rec += to_string (node->getNodeID ()) + " ";
				NodeStatus tNode = *node;
				Q.push_back (tNode);
			}
		}
		// Calc N
		N = (Q.begin ())->getRU ();
		DN = (Q.begin ())->getDN ();
		
		// Upper bound
		if (N % 2 != 0)
			N = N + 1;
		
		// Test 1: |Q| > (N / 2)
		if (Q.size () > (N / 2)) {
			grant = OKAY;
			record.append ("grant OKAY");
		}
		// Test 2: |Q| = (N / 2) && DN is an element of Q
		else if (Q.size () == (N / 2)) {
			// Check if DN is present in Q
			// Assume No_QUORUM
			grant = NO_QUORUM;
			for (vector<NodeStatus>::iterator node = Q.begin (); node < Q.end (); node++) {
				if (node->getNodeID () == DN) {
					grant = OKAY;
				}
			}
			
			if (grant == NO_QUORUM)
				record.append ("grant NO_QUORUM");
			else
				record.append ("grant OKAY");
		}
		else {
			// If no conditions satisfy, NO_QUORUM
			grant = NO_QUORUM;
			record.append ("grant NO_QUORUM");
		}
	}

	if (grant == OKAY) {
		// pthread_mutex_lock(&lockMutex);
		lock = SHARED;
		// pthread_mutex_unlock(&lockMutex);
		count++;
		record.append ("Count value incremented to " + to_string (count));
	}
		
	return grant;
}

int 	LockManager::getExclusiveLock () {
	int 	hSocket;
    struct 	hostent * 		pHostInfo;
    struct 	sockaddr_in 	Address;
    long 	nHostAddress;
	char 	msg [SIZE], temp [SIZE];
	char 	* str;
	
	P.clear ();
	Q.clear ();
	selfRequest = EXCLUSIVE;
	str = NULL;
	
	// check locker for any LOCKS
	pthread_mutex_lock(&lockerMutex);
	for (int i = 0; i < nodeCount; i++) {
		if (locker [i] != IDLE) {
			record.append ("Node " + to_string (i) + " is occupying " + to_string (locker [i]));
			pthread_mutex_unlock(&lockerMutex);
			return BUSY;
		}
	}
	pthread_mutex_unlock(&lockerMutex);
	
	// tell the object that I am sending messages and to FREEZE all other +ve responses on server by setting 'lock'
	
	// send messages to all nodes requesting for a lock
	for (int i = 0; i < nodeCount; i++) {
		if (i != nodeID) {
			hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (hSocket == SOCKET_ERROR)	{
				record.append ("getExclusiveLock: Could not make a socket");
				exit (1);
			}
			
			record.append ("Requesting Exclusive access from node " + to_string (nodes[i].getNodeID ()));
			
			pHostInfo = gethostbyname ((nodes[i].getHostName ()).c_str ());
			memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);

			Address.sin_addr.s_addr = 	nHostAddress;
			Address.sin_port 		= 	htons (nodes[i].getPort ());
			Address.sin_family 		= 	AF_INET;	
			
			if (connect (hSocket, (struct sockaddr *) &Address, sizeof (Address)) == SOCKET_ERROR)	{
				record.append ("getExclusiveLock: Failed to connect to " + to_string (nodes[i].getNodeID ()));
				// exit (1);
			}
			else {
				char * str = NULL;
				do {
					memset (msg, 0, SIZE);
					sprintf (msg, "REQUEST_EXCLUSIVE_LOCK|%d|%d#", this->getNodeID (), count);
					write (hSocket, msg, strlen (msg));

					record.append ("Message Sent to " + to_string (nodes[i].getNodeID ()) + " :  " + string (msg));
					
					memset (msg, 0, SIZE);
					read (hSocket, msg, SIZE);
			
					// as per the replies, create an object of NodeStatus, initialize it and push_back on P
			
					msg[SIZE] = '\0';
					if (!strcmp (msg, ""))
						break;
						
					record.append ("Message Received from " + to_string (nodes[i].getNodeID ()) + " :  " + string (msg));
					strcpy (temp, msg);
					
					str = NULL;
					str = strtok (temp, "|");
					
					// usleep (DELAY);
				} while (string (str) != "REQUEST_DENIED#" && string (str) != "EXCLUSIVE_LOCK_GRANTED");
				
				if (strcmp (msg, "") && string (str) == "EXCLUSIVE_LOCK_GRANTED") {
					int tRU, tVN, tDN;
					sscanf (msg, "EXCLUSIVE_LOCK_GRANTED|%d|%d|%d#", &tRU, &tVN, &tDN);
					
					NodeStatus ns = NodeStatus (nodes[i].getNodeID (), nodes[i].getHostName (), nodes[i].getPort ());
					ns.setRU (tRU);
					ns.setVN (tVN);
					ns.setDN (tDN);
					
					P.push_back (ns);
				}
			}
			
			// Close TCP Connection
			if (close (hSocket) == SOCKET_ERROR)	{
				record.append ("getExclusiveLock: Could not close a socket");
			}
		}
		else {
			NodeStatus ns = NodeStatus (getNodeID (), nodes[getNodeID ()].getHostName (), nodes[getNodeID ()].getPort ());
			ns.setRU (RU);
			ns.setVN (VN);
			ns.setDN (DN);
			
			P.push_back (ns);
		}
	}
	
	int grant = NONE;
	// check if I have my Quorum
	/*Algorithm: Based on the result, set grant to OKAY or NO_QUORUM*/
	
	if (P.size () > 1) {
		M = -1;
		for (vector<NodeStatus>::iterator node = P.begin (); node < P.end (); node++) {
			if (M < node->getVN ())
				M = node->getVN ();
		}
		
		// Compute Q
		string rec = "";
		Q.clear ();
		for (vector<NodeStatus>::iterator node = P.begin (); node < P.end (); node++) {
			if (node->getVN () == M) {
				rec += to_string (node->getNodeID ()) + " ";
				NodeStatus tNode = *node;
				Q.push_back (tNode);
			}
		}
		
		// Calc N
		N = (Q.begin ())->getRU ();
		DN = (Q.begin ())->getDN ();
		
		// Upper bound
		if (N % 2 != 0)
			N = N + 1;
		
		// Test 1: |Q| > (N / 2)
		if (Q.size () > (N / 2)) {
			grant = OKAY;
			record.append ("grant OKAY");
		}
		// Test 2: |Q| = (N / 2) && DN is an element of Q
		else if (Q.size () == (N / 2)) {
			// Check if DN is present in Q
			// Assume No_QUORUM
			grant = NO_QUORUM;
			for (vector<NodeStatus>::iterator node = Q.begin (); node < Q.end (); node++) {
				if (node->getNodeID () == DN) {
					grant = OKAY;
				}
			}
			
			if (grant == NO_QUORUM)
				record.append ("grant NO_QUORUM");
			else
				record.append ("grant OKAY");
		}
		else {
			// If no conditions satisfy, NO_QUORUM
			grant = NO_QUORUM;
			record.append ("grant NO_QUORUM");
		}
	}
	
	if (grant == OKAY) {
		count++;
		// pthread_mutex_lock(&lockMutex);
		lock = EXCLUSIVE;
		// pthread_mutex_unlock(&lockMutex);
	}
	
	return grant;
}

void	LockManager::performRead () {
	string rec = "Q = { ";
	for (vector<NodeStatus>::iterator node = Q.begin (); node < Q.end (); node++) {
		rec += to_string (node->getNodeID ()) + " ";
	}
	rec += "}; P = { ";
	for (vector<NodeStatus>::iterator node = P.begin (); node < P.end (); node++) {
		rec += to_string (node->getNodeID ()) + " ";
	}
	rec += "}";
	record.append ("Count " + to_string (count) + ": Shared Lock Acquired: M - " + to_string (M) + ", N - " + to_string (N) + "; " + rec);
	cout << "Node " << getNodeID () << " -- " << "Count " << count << ": Shared Lock Acquired: M - " + to_string (M) + ", N - " + to_string (N) + "; " + rec << endl;
	
	rec = "";
	if (VN < M) {
		rec = "Current node VN is " + to_string (VN) + ". Reading from the node " + to_string ((Q.begin ())->getNodeID ()) + " as it has VN = M (" + to_string (M) + ").";
	}
	else {
		rec = "Reading from Current node as VN = M (" + to_string (M) + ").";
	}
	record.append (rec);
	// cout << "Node " << getNodeID () << " -- " << "Count " << count << ": Shared Lock Acquired: " + rec << endl;
}

void	LockManager::performWrite () {
	string rec = "Q = { ";
	for (vector<NodeStatus>::iterator node = Q.begin (); node < Q.end (); node++) {
		rec += to_string (node->getNodeID ()) + " ";
	}
	rec += "}; P = { ";
	for (vector<NodeStatus>::iterator node = P.begin (); node < P.end (); node++) {
		rec += to_string (node->getNodeID ()) + " ";
	}
	rec += "}";
	record.append ("Count " + to_string (count) + ": Exclusive Lock Acquired: M - " + to_string (M + 1) + ", N - " + to_string (N) + "; " + rec);
	cout << "Node " << getNodeID () << " -- " << "Count " << count << ": Exclusive Lock Acquired: M - " + to_string (M + 1) + ", N - " + to_string (N) + "; " + rec << endl;
	
	// Update the current node's files to version M
	if (VN < M) {
		record.append ("Updating current set of files to version " + to_string (M));
		system (("/bin/cp ./" + to_string ((Q.begin ())->getNodeID ()) + "/* ./" + to_string (nodeID) + "/").c_str ());
	}
	
	// Append the current entry to the files of current node
	rec = "Node " + to_string (getNodeID ()) + " -- Count " + to_string (count) + ": M - " + to_string (M) + ", N - " + to_string (N) + "; " + rec;
	
	ofstream ofile;
	
	record.append ("Appending control file with updated version's data -- " + rec);
	ofile.open (("./" + to_string (getNodeID ()) + "/control").c_str (), ios::app);
	ofile << rec << endl;
	ofile.close ();
	
	record.append ("Appending " + to_string (count % nFiles) + " file with updated updated count " + to_string (count));
	ofile.open (("./" + to_string (getNodeID ()) + "/" + to_string (count % nFiles)).c_str (), ios::app);
	ofile << "Node " << getNodeID () << " -- Count " << count << endl;
	ofile.close ();
	
	// Propagate the updates to all nodes in P
	for (vector<NodeStatus>::iterator node = P.begin (); node < P.end (); node++) {
		if (node->getNodeID () != getNodeID ()) {
			record.append ("Propagating changes to Node " + to_string (node->getNodeID ()));
			system (("/bin/cp ./" + to_string (getNodeID ()) + "/control ./" + to_string (node->getNodeID ()) + "/").c_str ());
			system (("/bin/cp ./" + to_string (getNodeID ()) + "/" + to_string (count % nFiles) + " ./" + to_string (node->getNodeID ()) + "/").c_str ());
		}
	}
	
	// Propagate the changes in control file to Test
	system (("/bin/cp ./" + to_string (getNodeID ()) + "/control ./Test/").c_str ());
	
	RU = P.size ();
	VN = M + 1;
	DN = nodeID;
	
	record.append ("Count " + to_string (count) + ": Updated Variables (RU, VN, DN): (" + to_string (RU) + ", " + to_string (VN) + ", " + to_string (DN) + ")");
}

void	LockManager::releaseSharedLock () {
	// For all nodes in P, relase lock
	int 	hSocket;
    struct 	hostent * 		pHostInfo;
    struct 	sockaddr_in 	Address;
    long 	nHostAddress;
	char 	msg [SIZE];
	
	for (vector<NodeStatus>::iterator node = P.begin (); node < P.end (); node++) {
		if (node->getNodeID () != getNodeID ()) {
			hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (hSocket == SOCKET_ERROR)	{
				record.append ("releaseSharedLock: Could not make a socket");
				exit (1);
			}
			
			pHostInfo = gethostbyname ((node->getHostName ()).c_str ());
			memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);

			Address.sin_addr.s_addr = 	nHostAddress;
			Address.sin_port 		= 	htons (node->getPort ());
			Address.sin_family 		= 	AF_INET;	
			
			if (connect (hSocket, (struct sockaddr *) &Address, sizeof (Address)) == SOCKET_ERROR)	{
				record.append ("releaseSharedLock: Failed to connect to " + to_string (node->getNodeID ()));
				// exit (1);
			}
			else {
				char * str = NULL;
				do {
					memset (msg, 0, SIZE);
					sprintf (msg, "RELEASE_SHARED_LOCK|%d#", this->getNodeID ());
					write (hSocket, msg, strlen (msg));
					
					record.append ("Sending Message to " + to_string (node->getNodeID ()) + ": " + string (msg));

					memset (msg, 0, SIZE);
					read (hSocket, msg, SIZE);
					
					msg[SIZE] = '\0';
					record.append ("Receiving Message from " + to_string (node->getNodeID ()) + ": " + string (msg));
					
					str = NULL;
					str = strtok (msg, "#");
					
					// usleep (DELAY);
				} while (string (str) != "THANK_YOU");
			}
			
			// Close TCP Connection
			if (close (hSocket) == SOCKET_ERROR)	{
				record.append ("releaseSharedLock: Could not close a socket");
			}
		}
	}
	
	// pthread_mutex_lock(&lockMutex);
	lock = IDLE;
	// pthread_mutex_unlock(&lockMutex);
	selfRequest = NONE;
	P.clear ();
	Q.clear ();
}

void	LockManager::releaseExclusiveLock (int status) {
	// For all nodes in P, relase lock
	int 	hSocket;
    struct 	hostent * 		pHostInfo;
    struct 	sockaddr_in 	Address;
    long 	nHostAddress;
	char 	msg [SIZE];
	char	* str;
	
	str = NULL;
	
	for (vector<NodeStatus>::iterator node = P.begin (); node < P.end (); node++) {
		if (node->getNodeID () != getNodeID ()) {
			hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (hSocket == SOCKET_ERROR)	{
				record.append ("releaseExclusiveLock: Could not make a socket");
				exit (1);
			}
			
			pHostInfo = gethostbyname ((node->getHostName ()).c_str ());
			memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);

			Address.sin_addr.s_addr = 	nHostAddress;
			Address.sin_port 		= 	htons (node->getPort ());
			Address.sin_family 		= 	AF_INET;	
			
			if (connect (hSocket, (struct sockaddr *) &Address, sizeof (Address)) == SOCKET_ERROR)	{
				record.append ("releaseExclusiveLock: Failed to connect to " + to_string (node->getNodeID ()));
				// exit (1);
			}
			else {
				do {
				
					// Some more parameters need to be added
					if (status == OKAY) 
						sprintf (msg, "RELEASE_EXCLUSIVE_LOCK|%d|%d|%d|%d#", this->getNodeID (), this->getRU (), this->getVN (), this->getDN ());
					else
						sprintf (msg, "RELEASE_EXCLUSIVE_LOCK|%d|-1|-1|-1#", this->getNodeID ());
					
					record.append ("Sending Message to " + to_string (node->getNodeID ()) + ": " + string (msg));
					write (hSocket, msg, strlen (msg));

					memset (msg, 0, SIZE);
					read (hSocket, msg, SIZE);
					
					msg[SIZE] = '\0';
					
					if (!strcmp (msg, ""))
						break;
					record.append ("Receiving Message from " + to_string (node->getNodeID ()) + ": " + string (msg));
					
					str = NULL;
					str = strtok (msg, "#");
					
					// usleep (DELAY);
				} while (string (str) != "THANK_YOU");
			}
			
			// Close TCP Connection
			if (close (hSocket) == SOCKET_ERROR)	{
				record.append ("releaseExclusiveLock: Could not close a socket");
			}
		}
	}
	
	// pthread_mutex_lock(&lockMutex);
	lock = IDLE;
	// pthread_mutex_unlock(&lockMutex);
	selfRequest = NONE;
	P.clear ();
	Q.clear ();
}

void	LockManager::releaseFromDead (int nID) {
	pthread_mutex_lock(&lockerMutex);
	locker [nID] = IDLE;
	pthread_mutex_unlock(&lockerMutex);
}	

void 	LockManager::updateLocker (int nID, int locktype) {
	pthread_mutex_lock(&lockerMutex);
	locker [nID] = locktype;
	pthread_mutex_unlock(&lockerMutex);
}

void	LockManager::releaseLocker (int nID) {
	pthread_mutex_lock(&lockerMutex);
	locker [nID] = IDLE;
	pthread_mutex_unlock(&lockerMutex);
}

bool	LockManager::canGiveSharedLock (int tCount, int nID) {
	record.append ("Shared Lock: lock, selfRequest, (count, tCount), (nID, nodeID): " + to_string (lock) + ", " + to_string (selfRequest) + ", (" + to_string (count) + ", " + to_string (tCount) + "), (" + to_string (nID) + ", " + to_string (nodeID) + ")");
	
	// // pthread_mutex_lock(&lockMutex);
	if (lock == EXCLUSIVE)
		return false;
	// // pthread_mutex_unlock(&lockMutex);
		
	if (selfRequest != NONE) {
		if (tCount > count)
			return false;
		else if (tCount == count) {
			if (nID > nodeID)
				return false;
		}
	}
	
	pthread_mutex_lock(&lockerMutex);
	for (int i = 0; i < nodeCount; i++) {
		if (locker[i] == EXCLUSIVE) {
			pthread_mutex_unlock(&lockerMutex);
			return false;
		}
	}
	
	locker[nID] = SHARED;
	pthread_mutex_unlock(&lockerMutex);
	return true;
}

bool	LockManager::canGiveExclusiveLock (int tCount, int nID) {
	record.append ("Exclusive Lock: lock, selfRequest, (count, tCount), (nID, nodeID): " + to_string (lock) + ", " + to_string (selfRequest) + ", (" + to_string (count) + ", " + to_string (tCount) + "), (" + to_string (nID) + ", " + to_string (nodeID) + ")");
	// // pthread_mutex_lock(&lockMutex);
	if (lock != IDLE)
		return false;
	// // pthread_mutex_unlock(&lockMutex);
		
	if (selfRequest != NONE) {
		if (tCount > count)
			return false;
		else if (tCount == count) {
			if (nID > nodeID)
				return false;
		}
	}
	
	pthread_mutex_lock(&lockerMutex);
	for (int i = 0; i < nodeCount; i++) {
		if (locker[i] != IDLE) {
			pthread_mutex_unlock(&lockerMutex);
			return false;
		}
	}
	
	locker[nID] = EXCLUSIVE;
	pthread_mutex_unlock(&lockerMutex);
	return true;
}

int	LockManager::getNodeID () {
	return nodeID;
}

void * 	LockManager::isAlive (void * ptr) {
	LockManager * lm;
	Node * node;
	
	// TCP Variables
	int 	hSocket;
    struct 	hostent * 		pHostInfo;
    struct 	sockaddr_in 	Address;
    long 	nHostAddress;
	char 	msg [SIZE];
	
	lm = (LockManager *) ptr;
	
	while (true) {
		for (int i = 0; i < lm->getNodeCount (); i++) {
			if (i != lm->getNodeID ()) {
				node = lm->getNode (i);
				
				// Establish TCP connection with node
				hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
				if (hSocket == SOCKET_ERROR)	{
					lm->record.append ("isAlive: Could not make a socket");
					continue;
				}
				
				pHostInfo = gethostbyname ((node->getHostName ()).c_str ());
				memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);

				Address.sin_addr.s_addr = 	nHostAddress;
				Address.sin_port 		= 	htons (node->getPort ());
				Address.sin_family 		= 	AF_INET;	
				
				if (connect (hSocket, (struct sockaddr *) &Address, sizeof (Address)) == SOCKET_ERROR)	{
					node->setLife (DEAD);
					lm->releaseFromDead (node->getNodeID ());
					lm->record.append ("isAlive thread declaring " + to_string (node->getNodeID ()) + ": DEAD");
				}
				else {
					sprintf (msg, "ARE_YOU_ALIVE#");
					write (hSocket, msg, strlen (msg));
					
					// lm->record.append ("isAlive thread sending message to " + to_string (node->getNodeID ()) + ": " + string (msg));
					
					memset (msg, 0, SIZE);
					read (hSocket, msg, SIZE);
					
					msg[SIZE] = '\0';
					
					try {
						// lm->record.append ("isAlive thread receiving message from " + to_string (node->getNodeID ()) + ": " + string (msg));

						char * str = NULL;
						str = strtok (msg, "#");
						
						if (string (str) == "I_AM_ALIVE") {
							node->setLife (ALIVE);
							// lm->record.append ("isAlive thread declaring " + to_string (node->getNodeID ()) + ": ALIVE");
						}
						else {
							node->setLife (DEAD);
							lm->releaseFromDead (node->getNodeID ());
							lm->record.append ("isAlive thread declaring " + to_string (node->getNodeID ()) + ": DEAD");
						}
					}
					catch (const exception& e) {
						node->setLife (DEAD);
						lm->releaseFromDead (node->getNodeID ());
						lm->record.append ("Exception Occurred - isAlive thread declaring " + to_string (node->getNodeID ()) + ": DEAD");
					}
				}
				
				// Close TCP Connection
				if (close (hSocket) == SOCKET_ERROR)	{
					lm->record.append ("isAlive: Could not close a socket");
				}
			}
		}
		usleep (DELAY * 1000);
	}
}