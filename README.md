AOS Project 3 - Implementation of a replicated file system using the dynamic voting protocol.
==
Project Coded in C++
--

#### Please follow the steps for execution:


**Step 1**: To compile, log on to `CS1/CS2.utdallas.edu` machines ONLY. This is 
		because the DC Machines DO NOT HAVE TCP Libraries to be linked for C/C++.

**Step 2**: Create your own configuration file in the format:

	# A line prefixed with '#' is treated as a comment
	# But the order in which the information is placed matters
	# First the total Number of Nodes
	<An Integer>
	# Then the list of Node IDs, it's hostname and port number
	<NODE ID1>  <HOSTNAME>  <PORT>  
	<NODE ID2>  <HOSTNAME>  <PORT>  
	..
	# Total number of files in the file system
	<An Integer>
	# Number of operations executed per node
	<An Integer>
	# Mean delay between two consecutive operations (assume exponentially distributed)
	<An Integer - treated as milliseconds>
	#Fraction of read operations
	<An Integer>
	# Parameters of exponential backoff (minimum and maximum waiting times)
	< 2 Integers - treated as milliseconds>
    
Save the filename as "config" or some custom name.

**Step 3**: If a custom file (name which is not "config") is created, edit 
		"initialize.sh", and change the "configFile" to your custom name.

**Step 4**: Edit the "**initialize.sh**" for few more parameters:

- User Executing the code should replace his/her NetID against the value of "remoteuser" for successful 
   execution. (Unless it is the Super User.)

- MAKE SURE THAT THE (NODE ID, HOSTNAME)s LISTED ON THE CONFIG FILE
   ARE LISTED AS SAME (VALUE AND ORDER) IN THE "remotecomputer" variables in the initialize.sh.
		   
**Step 5**: Compile

	>*location_for_all_the_files_listed_in_Note_2_below*
	> make /*Edit the contents of Makefile if necessary*/
    	   
**Step 6**: Execution

	Run the command:
	> sh initialize.sh

**Step 7**: Output -- The Output can be seen on screen (mixed with all node processes) or can also be 
		viewed in a file for each node under the directory Logs. Output file Names is after the <NODE ID> 
		Example, if the node ID is "1", the output file created is also "1" By, "cat 1", you can view 
		the output logs.

TESTING
	Nodes that have missed out on updates and the nodes which share same file system can be seen like this

		> sh test.sh <Number of nodes>

	The testing output can be seen on the screen.
		
#################################################################################
Note:

1. If you want to compile the code manually (note with the initialize.sh script) then,

`g++491 --std=c++11 -Wl,-rpath=/usr/local/gcc491/lib64 -lpthread log.cpp application.cpp lock_manager.cpp -o node` 
  
2. Make sure that the following files are in the same folder for successful
   execution
   	- initialize.sh 
	- log.cpp
	- log.h
	- application.cpp
	- application.h
	- lock_manager.cpp
	- lock_manager.h
	- clear.sh
	- test.sh
	- Makefile
	- config "or" custom_knowledge_graph_file 

3. Nodes can be created or killed dynamically.
#################################################################################

*Please contact me on any kind of concerns/questions*

**By,** 
### Tejas Tovinkere Pattabhi 
**Email: tejas.pattabhi@gmail.com**

**Net ID: txp130630**