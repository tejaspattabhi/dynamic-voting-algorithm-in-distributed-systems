#ifndef APPLICATION_H
#define APPLICATION_H

#include <iostream>
#include <fstream>
using namespace std;
#include <random>
#include <queue>
#include <string>
#include <cstring>

#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <pthread.h>

#include "log.h"

#define	SIZE			100
#define	SOCKET_ERROR	-1

#define	LOCKED			1
#define	UNLOCKED		0

#define	ALIVE			1
#define	DEAD			0

#define	READ			1
#define	WRITE			0
#define NONE			-1

#define	BUSY			10
#define	OKAY			11
#define	NO_QUORUM		12

class Application {
	private:
		LogFile	record;
		int	mDelay;
		int nodeID;
		
		int	dDelay [SIZE * SIZE];
		int	nOperations;
		int	fWriteCalls;
		int	nFiles;
		
		int	minWait, maxWait;
		
		void exponentialDistribution (int);	
		
	public:
		Application ();
		Application (LogFile, int, string = "config");
		
		int	getDelay (int);
		int getNumberOfOperations ();
		int	readOrWrite (int);
		int getOperation (int);
		
		int getMinWait ();
		int getMaxWait ();
};

#endif