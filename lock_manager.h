#ifndef LOCK_MANAGER_H
#define LOCK_MANAGER_H

#include <iostream>
#include <fstream>
using namespace std;
#include <mutex>
#include <random>
#include <queue>
#include <string>
#include <cstring>
#include <chrono>
#include <algorithm>

#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <pthread.h>

#include "log.h"

#define	SIZE			100
#define	SOCKET_ERROR	-1

#define	ALIVE			1
#define	DEAD			0

#define	SHARED			1
#define	EXCLUSIVE		2
#define	IDLE			0

#define	READ			1
#define	WRITE			0
#define NONE			-1

class Node {
	protected:
		int 	nodeID;
		string 	hostname;
		int 	port;
		volatile	int		life;
	
	public:
		Node ();
		Node (int);
		Node (int, string, int);
		
		int 	getNodeID ();
		string 	getHostName ();
		int 	getPort ();
		
		void 	setLife (int);
		int		getLife ();
};

class NodeStatus : public Node {
	private:
		int		RU, VN, DN;
	public:
		NodeStatus ();
		NodeStatus (int, string, int);
		NodeStatus (int, int, int);
		int		getRU ();
		void	setRU (int);
		int 	getVN ();
		void 	setVN (int);
		int 	getDN ();
		void 	setDN (int);
};

class LockManager {
	private:
		Node				nodes [SIZE];
		volatile int 		selfRequest;
		vector<NodeStatus>	P, Q;
		volatile int		M, N, QCount;
		volatile int		RU, VN, DN;
		
		pthread_mutex_t 	lockerMutex;
		int					locker [SIZE];
		
		pthread_mutex_t 	lockMutex;
		volatile int		lock;
		
		int					nodeID;
		string				hostname;
		int					port;
		
		int					nFiles;
		volatile int		count;
		int 				nodeCount;
		
	public:
		LogFile		record;
		LockManager ();
		LockManager (LogFile, int, string = "config");
		
		static	void * 	serverThread (void *);
		static	void * 	isAlive (void *);
		
		int		getNodeID ();
		int		getMyPort ();
		string	getMyHostName ();
		int		getNodeCount ();
		Node *	getNode (int);
		
		void	releaseFromDead (int);
		void 	updateLocker (int, int);
		void	releaseLocker (int);
		
		int 	getSharedLock ();
		int 	getExclusiveLock ();
		
		void	performRead ();
		void	performWrite ();
		
		void	releaseSharedLock ();
		void	releaseExclusiveLock (int);
		
		bool	canGiveSharedLock (int, int);
		bool	canGiveExclusiveLock (int, int);
		
		int		getRU ();
		void	setRU (int);
		int 	getVN ();
		void 	setVN (int);
		int 	getDN ();
		void 	setDN (int);
};

#endif