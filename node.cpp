#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>
using namespace std;

#include "log.h"
#include "lock_manager.h"
#include "application.h"

#define	SIZE			100
#define	SOCKET_ERROR	-1

#define	LOCKED			1
#define	UNLOCKED		0

#define	ALIVE			1
#define	DEAD			0

#define	READ			1
#define	WRITE			0
#define NONE			-1

#define	BUSY			10
#define	OKAY			11
#define	NO_QUORUM		12

#define	EXPO_WAIT		50000

/*Class and Member functions of Application*/
int main (int argc, char * argv []) {
	Application app;
	LockManager lm;
	LogFile record;
	int nodeID;
	string fName;
	
	int			thID;
	pthread_t	server, alive;
	
	if (argc < 2) {
		cout << "Usage: " << argv[0] << " <NODE ID> [<Config File Name>]" << endl;
		return 1;
	}
	else if (argc == 2) {
		fName = "config";
	}
	if (argc == 3) {
		fName = string (argv [2]);
	}
	else {
		cout << "Usage: " << argv[0] << " <NODE ID> [<Config File Name>]" << endl;
		return 1;
	}
	
	nodeID = atoi (argv[1]);
	record = LogFile (nodeID, "./Logs/" + string (argv[1]));
	app = Application (record, nodeID, fName);
	lm = LockManager (record, nodeID, fName);
	
	thID = pthread_create (&server, NULL, &LockManager::serverThread, (void *) &lm);
	if (thID) {
		record.append ("Error Creating a thread. Dying...");
		exit (1);
	}
	sleep (1);
	
	thID = pthread_create (&server, NULL, &LockManager::isAlive, (void *) &lm);
	if (thID) {
		record.append ("Error Creating a thread isAlive. Dying...");
		exit (1);
	}
	sleep (2);
	
	// Application Actions
	for (int i = 0; i < app.getNumberOfOperations (); i++) {
		if (app.getOperation (i) == READ) {
			record.append (to_string (i) + " iteration -- Shared Lock Request");
			int response = lm.getSharedLock ();
			while (response != OKAY) {
				lm.releaseSharedLock ();
				
				// sleep (app.getMaxWait ());
				usleep (EXPO_WAIT);
				response = lm.getSharedLock ();
			}
			
			// got Quorum
			lm.performRead ();
			lm.releaseSharedLock ();
		}	
		else { // WRITE 
			record.append (to_string (i) + " iteration -- Exclusive Lock Request");
			int response = lm.getExclusiveLock ();
			while (response != OKAY) {
				lm.releaseExclusiveLock (BUSY);
				
				// sleep (app.getMaxWait ());
				usleep (EXPO_WAIT);
				response = lm.getExclusiveLock ();
			}
			
			// got Quorum
			lm.performWrite ();
			lm.releaseExclusiveLock (OKAY);
		}
		
		usleep (app.getDelay (i) * 100);
	}
	
	sleep (100);
	return 0;
}