CXX = g++491
CXXFLAGS = --std=c++11
LDFLAGS = -Wl,-rpath=/usr/local/gcc491/lib64 -lpthread
OBJFILES = node.o log.o application.o lock_manager.o
EXE = node

all: $(EXE)

$(EXE): $(OBJFILES) 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^