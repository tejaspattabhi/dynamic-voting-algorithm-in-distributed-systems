#!/bin/sh

i=0
while [ "$i" -lt 10 ]  
do
	rm -Rf "$i"
	mkdir "$i"

	i=`expr $i + 1`
done

rm -Rf Logs
mkdir Logs

rm -Rf Test
mkdir Test

remoteuser=txp130630
remotecomputer1=dc17
remotecomputer2=dc16
remotecomputer3=dc24
remotecomputer4=dc29
remotecomputer5=dc14
remotecomputer6=dc43
remotecomputer7=dc21
remotecomputer8=dc15
remotecomputer9=dc44
remotecomputer10=dc13

# ConfigFile Custom name
configFile=trial1

ssh -l "$remoteuser" "$remotecomputer1" "cd $HOME/AOS/Project;./node 0 $configFile" &
ssh -l "$remoteuser" "$remotecomputer2" "cd $HOME/AOS/Project;./node 1 $configFile" &
ssh -l "$remoteuser" "$remotecomputer3" "cd $HOME/AOS/Project;./node 2 $configFile" &
ssh -l "$remoteuser" "$remotecomputer4" "cd $HOME/AOS/Project;./node 3 $configFile" &
ssh -l "$remoteuser" "$remotecomputer5" "cd $HOME/AOS/Project;./node 4 $configFile" &
ssh -l "$remoteuser" "$remotecomputer6" "cd $HOME/AOS/Project;./node 5 $configFile" &
ssh -l "$remoteuser" "$remotecomputer7" "cd $HOME/AOS/Project;./node 6 $configFile" &
ssh -l "$remoteuser" "$remotecomputer8" "cd $HOME/AOS/Project;./node 7 $configFile" &
ssh -l "$remoteuser" "$remotecomputer9" "cd $HOME/AOS/Project;./node 8 $configFile" &
ssh -l "$remoteuser" "$remotecomputer10" "cd $HOME/AOS/Project;./node 9 $configFile" &

