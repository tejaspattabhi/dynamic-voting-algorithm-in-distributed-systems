#include <iostream>
#include <fstream>
using namespace std;
#include <random>
#include <queue>
#include <string>
#include <cstring>

#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <pthread.h>

#include "log.h"

#define	SIZE			100
#define	SOCKET_ERROR	-1

#define	LOCKED			1
#define	UNLOCKED		0

#define	ALIVE			1
#define	DEAD			0

#define	READ			1
#define	WRITE			0

/*LogFile Member Functions*/
LogFile::LogFile () {
	nodeID = -1;
	fName = "";
}

LogFile::LogFile (int nID, string file) {
	ofstream ofile; 
	
	nodeID = nID;
	fName = file;
	
	// Fflush
	ofile.open (fName.c_str ());
	ofile.close ();
}

void LogFile::append (string msg) {
	ofstream ofile;

	ofile.open (fName.c_str (), std::ios::app);
	// cout << "Node " << nodeID << " -- " << msg << endl;
	ofile << "Node " << nodeID << " -- " << msg << endl;
	ofile.close ();
}