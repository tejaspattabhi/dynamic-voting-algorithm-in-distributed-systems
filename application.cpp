#include <iostream>
#include <fstream>
using namespace std;
#include <random>
#include <queue>
#include <string>
#include <cstring>
#include <chrono>
#include <algorithm>

#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <pthread.h>

#include "log.h"
#include "application.h"

#define	SIZE			100
#define 	SOCKET_ERROR		-1

#define	LOCKED			1
#define	UNLOCKED		0

#define	ALIVE			1
#define	DEAD			0

#define	READ			1
#define	WRITE			0

void Application::exponentialDistribution (int avg) {
	float 	x;

	x 	= 	(float) (1 / (avg * 1.0));
	std::default_random_engine generator;
	std::exponential_distribution<double> distribution (x);
	
	for (int i = 0; i < nOperations; ++i) {
		double number = distribution(generator);
		while (number < 50.0)
			number *= 2;
		dDelay [i] = int (number);
	}
	
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	shuffle (&dDelay [0], &dDelay [nOperations - 1], std::default_random_engine(seed));
	
	string s = "";
	for (int i = 0; i < nOperations; ++i) {
		s += " " + to_string (dDelay [i]);
	}
	
	// record.append ("Random Numbers" + s);
	// cout << "Random Numbers" + s << endl;
}

Application::Application () {
	mDelay = nOperations = fWriteCalls = minWait = maxWait = 0;
	nodeID = -1;
	nFiles = 0;
}

Application::Application (LogFile rec, int nodeID, string configFile) {
	ifstream 	ifile;
	char 		buffer [SIZE * 5], temp [SIZE], value [SIZE];
	int 		i;
	
	// record = rec;
	this->nodeID = nodeID;
	ifile.open (configFile);
	
	// Read Config File
	ifile >> buffer;
	if (ifile.eof ()) {
		// record.append ("Config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			// record.append ("config file error");
			exit (1);
		}		
	}
	
	// buffer has node count
	int nodeCount = atoi (buffer);
	// record.append ("Node Count - " + to_string (nodeCount));
	
	ifile >> buffer;
	if (ifile.eof ()) {
		// record.append ("config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			// record.append ("config file error");
			exit (1);
		}		
	}
	
	ifile >> temp >> value;
	if (ifile.eof ()) {
		// record.append ("config file error");
		exit (1);
	}
	
	// record.append ("Node 0: " + string (buffer) + ", " + string (temp) + ", " + string (value));
	// Node information: Not required
	for (i = 1; i < nodeCount; i++) {
		ifile >> buffer >> temp >> value;
		// record.append ("Node " + to_string (i) + ": " + string (buffer) + ", " + string (temp) + ", " + string (value));
		if (ifile.eof ()) {
			// record.append ("config file error");
			exit (1);
		}
	}
	
	ifile >> buffer;
	if (ifile.eof ()) {
		// record.append ("config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			// record.append ("config file error");
			exit (1);
		}		
	}
	
	// buffer has number of files in the system now.
	// Data for application from here:
	nFiles = atoi (buffer);
	// record.append ("Number of Files: " + to_string (nFiles));
	
	ifile >> buffer;
	if (ifile.eof ()) {
		// record.append ("config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			// record.append ("config file error");
			exit (1);
		}		
	}
	
	nOperations = atoi (buffer);
	// record.append ("Number of Operations - " + to_string (nOperations));
	
	ifile >> buffer;
	if (ifile.eof ()) {
		// record.append ("config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			// record.append ("config file error");
			exit (1);
		}		
	}
	
	mDelay = atoi (buffer);
	// record.append ("Average Delay - " + to_string (mDelay));
	exponentialDistribution (mDelay);	
	
	ifile >> buffer;
	if (ifile.eof ()) {
		// record.append ("config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			// record.append ("config file error");
			exit (1);
		}		
	}
	
	int readFraction = atoi (buffer);
	fWriteCalls = readFraction / (100 - readFraction);
	// record.append ("Write Calls - " + to_string (fWriteCalls));
	
	ifile >> buffer;
	if (ifile.eof ()) {
		// record.append ("config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer >> value;
		if (ifile.eof ()) {
			// record.append ("config file error");
			exit (1);
		}		
	}
	
	minWait = atoi (buffer);
	maxWait = atoi (value);
	
	// record.append ("Max and min waiting time - " + to_string (minWait) + ", " + to_string (maxWait));
	
	ifile.close ();
	
	ofstream ofile;
	for (int i = 0; i < nodeCount; i++) {
		ofile.open (("./" + to_string (i) + "/control").c_str ());
		ofile.close ();
		for (int j = 0; j < nFiles; j++) {
			ofile.open (("./" + to_string (i) + "/" + to_string (j)).c_str ());
			ofile.close ();
		}
	}
}

int Application::getDelay (int i) {
	return dDelay [i];
}

int Application::getNumberOfOperations () {
	return nOperations;
}

int Application::readOrWrite (int i) {
	if (i % fWriteCalls == 0)
		return WRITE;
	else
		return READ;
}

int Application::getMinWait () {
	return minWait;
}

int Application::getMaxWait () {
	return maxWait;
}

int Application::getOperation (int i) {
	if (i % fWriteCalls == 0)
		return WRITE;
	else
		return READ;
}