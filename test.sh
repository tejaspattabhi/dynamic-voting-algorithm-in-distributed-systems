#! /bin/bash

if [ "$#" != "1" ]
then
	echo "Usage: $0 <Number of Nodes>"
	exit
fi

node=0
while [ "$node" -lt $1 ]
do
	result=$(diff -a --suppress-common-lines ./Test/control ./$node/control)

	if [ $? -eq 0 ]
	then
       		echo "Node $node: Latest version of the file system."
	else
       		echo "Node $node: Not updated or has missed some updates."
		echo "The differences seen with the latest version:"
       		echo "$result"
	fi
	node=`expr $node + 1`
done
